﻿using GalaSoft.MvvmLight.Command;
using MasterServices.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace MasterServices.ViewModels
{
    public class QueriesViewModel : INotifyPropertyChanged
    {
        #region Properties

        public DateTime DateService { get; set; }

        private string _TotalValue;
        public string TotalValue
        {
            get
            {
                return _TotalValue;
            }
            set
            {
                _TotalValue = value;
                OnPropertyChanged("TotalValue");
            }
        }
        private List<Service> _Services;
        public List<Service> Services
        {
            get
            {
                return _Services;
            }
            set
            {
                _Services = value;
                OnPropertyChanged("Services");
            }
        }
        #endregion

        #region Attributes
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructors
        public QueriesViewModel()
        {
            DateService = DateTime.Today;
            TotalValue = "0";
            if (Services != null)
            {
                Services.Clear();
            }
        }
        #endregion

        #region Commands
        public ICommand QueryCommand { get { return new RelayCommand(Query); } }

        private void Query()
        {
            using (var da = new DataAccess())
            {
                var list = da.GetList<Service>(true)
                                .Where(s => s.DateService.Year == DateService.Date.Year &&
                                            s.DateService.Month == DateService.Date.Month &&
                                            s.DateService.Day == DateService.Date.Day)
                            .ToList();
                var total = list.Sum(l => l.Value);
                Services = list;
                TotalValue = string.Format("{0:C2}", total);
            }
        }

        #endregion

        #region Methods
        public void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
