﻿using GalaSoft.MvvmLight.Command;
using MasterServices.Services;
using System.Windows.Input;

namespace MasterServices.ViewModels
{
    public class MenuItemViewModel
    {
        #region Attributes
        private NavigationService navigationService;
        #endregion

        #region Constructors
        public MenuItemViewModel()
        {
            navigationService = new NavigationService();
        }
        #endregion

        #region Properties
        public string Icon { get; set; }

        public string Title { get; set; }

        public string PageName { get; set; }
        #endregion

        #region Commands
        public ICommand NavigateCommand { get { return new RelayCommand(() => navigationService.Navigate(PageName)); } }
        #endregion

    }
}
