﻿using GalaSoft.MvvmLight.Command;
using MasterServices.Models;
using MasterServices.Services;
using System.Linq;
using System.Windows.Input;

namespace MasterServices.ViewModels
{
    public class ProductViewModel : Product
    {

        #region Attributes
        private DialogService dialogService;
        #endregion

        #region Constructors
        public ProductViewModel()
        {
            dialogService = new DialogService();
        }
        #endregion

        #region Commands
        public ICommand SaveCommand { get { return new RelayCommand(Save); } }

        private async void Save()
        {
            try
            {
                if (string.IsNullOrEmpty(Description))
                {
                    await dialogService.ShowMessage("Error", "Debe ingresar una descripción");
                    return;
                }
                if (Price < 0 || Price == 0)
                {
                    await dialogService.ShowMessage("Error", "El precio debe ser mayor a cero");
                    return;
                }
                var product = new Product
                {
                    Description = Description,
                    Price = Price,
                };

                using (var da = new DataAccess())
                {
                    var descripcionProduct = da.GetList<Product>(false).Where(p => p.Description == Description).FirstOrDefault();
                    if (descripcionProduct != null)
                    {
                        await dialogService.ShowMessage("Error", "Ya existe un producto con esta descripcion");
                        return;
                    }

                    da.Insert(product);
                }
                await dialogService.ShowMessage("Información", "El producto ha sido creado");
            }
            catch 
            {
                await dialogService.ShowMessage("Error", "Ha ocurrido un error inesperado");
            }
        }
        #endregion
    }
}
