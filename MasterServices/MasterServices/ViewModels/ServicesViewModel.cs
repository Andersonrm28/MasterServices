﻿using GalaSoft.MvvmLight.Command;
using MasterServices.Models;
using MasterServices.Services;
using System;
using System.Windows.Input;

namespace MasterServices.ViewModels
{
    public class ServicesViewModel : Service
    {
        #region Attributes
        private DialogService dialogService;
        private Product product;
        #endregion

        #region Constructors
        public ServicesViewModel()
        {
            dialogService = new DialogService();
            DateService = DateTime.Today;
        }
        #endregion

        #region Commands
        public ICommand SaveCommand { get { return new RelayCommand(Save); } }

        private async void Save()
        {
            try
            {
                var porductId = Convert.ToString(ProductId);
                if (string.IsNullOrEmpty(porductId))
                {
                    await dialogService.ShowMessage("Error", "Debe ingresar un codigo de producto");
                    return;
                }
                if (Quantity < 0 || Quantity == 0)
                {
                    await dialogService.ShowMessage("Error", "La cantidad debe ser mayor a cero");
                    return;
                }

                using (var da = new DataAccess())
                {
                    product = da.Find<Product>(ProductId, false);
                    if (product == null)
                    {
                        await dialogService.ShowMessage("Error", "El codigo de producto ingresado no existe");
                        return;
                    }
                }

                var service = new Service
                {
                    DateService = DateService,
                    DateRegistered = DateTime.Today,
                    Price = product.Price,
                    ProductId = ProductId,
                    Quantity = Quantity,
                };

                using (var da = new DataAccess())
                {
                    da.Insert(service);
                }

                await dialogService.ShowMessage("Información", "El servicio ha sido creado");
            }
            catch
            {
                await dialogService.ShowMessage("Error", "Ha ocurrido un error inesperado");
            }
        }
        #endregion
    }
}
