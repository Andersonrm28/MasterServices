﻿using MasterServices.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MasterServices.Services;
using System.Linq;
using System.ComponentModel;
using Xamarin.Forms;

namespace MasterServices.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Attributes
        private NavigationService navigationService;

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        public ObservableCollection<MenuItemViewModel> Menu { get; set; }

        private ObservableCollection<ServicesViewModel> _Services;
        public ObservableCollection<ServicesViewModel> Services
        {
            get
            {
                return _Services;
            }
            set
            {
                _Services = value;
                OnPropertyChanged("Services");
            }
        }
        private ObservableCollection<ProductViewModel> _Products;
        public ObservableCollection<ProductViewModel> Products
        {
            get
            {
                return _Products;
            }
            set
            {
                _Products = value;
                OnPropertyChanged("Products");
            }
        }

        private Command loadTweetsCommand;

        public Command LoadTweetsCommand
        {
            get
            {
                return loadTweetsCommand ?? (loadTweetsCommand = new Command(ExecuteLoadTweetsCommand, () =>
                {
                    return !IsBusy;
                }));
            }
        }

        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                if (isBusy == value)
                    return;

                isBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        public ProductViewModel NewProduct { get; private set; }
        public ServicesViewModel NewService { get; private set; }
        public QueriesViewModel NewQuery { get; private set; }

        #endregion

        #region Constructors
        public MainViewModel()
        {
            navigationService = new NavigationService();
            Products = new ObservableCollection<ProductViewModel>();
            Services = new ObservableCollection<ServicesViewModel>();
            NewQuery = new QueriesViewModel();

            LoadMenu();
            LoadProducts();
            LoadServices();
        }

        #endregion

        #region Commands

        public ICommand GoToCommand { get { return new RelayCommand<string>(GoTo); } }

        private void GoTo(string pageName)
        {
            switch (pageName)
            {
                case "NewProductPage":
                    NewProduct = new ProductViewModel();
                    break;
                case "NewServicePage":
                    NewService = new ServicesViewModel();
                    break;
                default:
                    break;
            }

            navigationService.Navigate(pageName);
        }

        #endregion

        #region Methods
        private void LoadServices()
        {
            using (var da = new DataAccess())
            {
                var services = da.GetList<Service>(true)
                                    .OrderByDescending(s => s.DateService)
                                    .ToList();

                Services.Clear();
                if (services.Count() != 0)
                {
                    foreach (var service in services)
                    {
                        Services.Add(new ServicesViewModel
                        {
                            ProductId = service.ProductId,
                            DateService = service.DateService,
                            DateRegistered = service.DateRegistered,
                            Price = service.Price,
                            ServiceId = service.ServiceId,
                            Quantity = service.Quantity,
                            Product = service.Product,
                        });
                    }
                }
            }
        }

        private void LoadMenu()
        {
            Menu = new ObservableCollection<MenuItemViewModel>();

            Menu.Add(new MenuItemViewModel
            {
                Icon = "ic_action_create.png",
                PageName = "ProductsPage",
                Title = "Productos",
            });

            Menu.Add(new MenuItemViewModel
            {
                Icon = "ic_action_queries.png",
                PageName = "QueriesPage",
                Title = "Consultas",
            });

        }

        private void LoadProducts()
        {
            using (var da = new DataAccess())
            {
                var products = da.GetList<Product>(false).OrderBy(p => p.ProductId);
                Products.Clear();
                if (products.Count() != 0)
                {
                    foreach (var product in products)
                    {
                        Products.Add(new ProductViewModel
                        {
                            ProductId = product.ProductId,
                            Description = product.Description,
                            Price = product.Price,
                        });
                    }
                }
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void ExecuteLoadTweetsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            LoadTweetsCommand.ChangeCanExecute();
            LoadProducts();
            LoadServices();

            IsBusy = false;
            LoadTweetsCommand.ChangeCanExecute();
        }
        #endregion
    }
}
