﻿using SQLite.Net.Interop;

namespace MasterServices.Interfaces
{
    public interface IConfig
    {
        string DirectoryDB { get; }
        ISQLitePlatform Platform { get; }
    }
}
