﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.ComponentModel;

namespace MasterServices.Models
{
    public class Service : INotifyPropertyChanged
    {
        #region Properties
        private int _ServiceId;
        [PrimaryKey, AutoIncrement]
        public int ServiceId
        {
            get
            {
                return _ServiceId;
            }
            set
            {
                _ServiceId = value;
                OnPropertyChanged("ServiceId");
            }
        }

        private DateTime _DateService;
        public DateTime DateService
        {
            get
            {
                return _DateService;
            }
            set
            {
                _DateService = value;
                OnPropertyChanged("DateService");
            }
        }

        private DateTime _DateRegistered;
        public DateTime DateRegistered
        {
            get
            {
                return _DateRegistered;
            }
            set
            {
                _DateRegistered = value;
                OnPropertyChanged("DateRegistered");
            }
        }

        private int _ProductId;
        public int ProductId
        {
            get
            {
                return _ProductId;
            }
            set
            {
                _ProductId = value;
                OnPropertyChanged("ProductId");
            }
        }

        private Product _Product;
        [ManyToOne]
        public Product Product
        {
            get
            {
                return _Product;
            }
            set
            {
                _Product = value;
                OnPropertyChanged("Product");
            }
        }

        private decimal _Price;
        public decimal Price
        {
            get
            {
                return _Price;
            }
            set
            {
                _Price = value;
                OnPropertyChanged("Price");
            }
        }

        private double _Quantity;
        public double Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                _Quantity = value;
                OnPropertyChanged("Quantity");
            }
        }

        public decimal Value { get { return Price * (decimal)Quantity; } }

        public override int GetHashCode()
        {
            return ServiceId;
        }

        public override string ToString()
        {
            return string.Format("{0} {1:d} {2:C2}", ServiceId, DateService, Value);
        }
        #endregion

        #region Attributes
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods
        public void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
