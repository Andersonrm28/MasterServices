﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;
using System.ComponentModel;

namespace MasterServices.Models
{
    public class Product : INotifyPropertyChanged
    {
        #region Properties
        private int _ProductId;
        [PrimaryKey, AutoIncrement]
        public int ProductId
        {
            get
            {
                return _ProductId;
            }
            set
            {
                _ProductId = value;
                OnPropertyChanged("ProductId");
            }
        }

        private string _Description;
        [Unique]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
                OnPropertyChanged("Description");
            }
        }

        private decimal _Price;
        public decimal Price
        {
            get
            {
                return _Price;
            }
            set
            {
                _Price = value;
                OnPropertyChanged("Price");
            }
        }

        
        private List<Service> _Services;
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Service> Services
        {
            get
            {
                return _Services;
            }
            set
            {
                _Services = value;
                OnPropertyChanged("Services");
            }
        }

        public override int GetHashCode()
        {
            return ProductId;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2:C2}", ProductId, Description, Price);
        }
        #endregion

        #region Attributes
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods
        public void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
